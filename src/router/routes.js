import RenderMD from "components/RenderMD"

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/one',
        component: RenderMD,
        props: {
          title: 'One',
          hello: true,
          filename: 'subdir/one.md',
          generateTOC: true
        }
      },
      {
        path: '/two',
        component: RenderMD,
        props: {
          title: 'Two',
          hello: true,
          filename: 'subdir/two.md',
          generateTOC: false
        }
      },
    ]
  },


  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
